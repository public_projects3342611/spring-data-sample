package ru.nazarenko.springdata.repository;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nazarenko.springdata.model.UserDao;

import java.time.ZoneId;

@Component
public class DataInitializer {

    private final UserRepository userRepository;

    @Autowired
    public DataInitializer(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void initializeData() {
        Faker faker = new Faker();

        for (int i = 0; i < 10; i++) {
            UserDao userDao = new UserDao();

            userDao.setName(faker.name().firstName());
            userDao.setSurname(faker.name().lastName());
            userDao.setDateOfBirth(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());

            userRepository.save(userDao);
        }
    }
}

