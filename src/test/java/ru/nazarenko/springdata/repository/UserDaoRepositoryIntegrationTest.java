package ru.nazarenko.springdata.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Import;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nazarenko.springdata.config.AppConfiguration;
import ru.nazarenko.springdata.model.UserDao;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
@Import(DataInitializer.class)
class UserDaoRepositoryIntegrationTest {

    @Autowired
    private DataInitializer dataInitializer;

    @Autowired
    private UserRepository userRepository;

    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            AppConfiguration.DATABASE_DOCKER_IMAGE
    );

    @Test
    void establishConnectionWithDbTest(){
        assertTrue(postgres.isCreated());
        assertTrue(postgres.isRunning());
    }

    @BeforeEach
    void setUp() {
        dataInitializer.initializeData();
    }

    @AfterEach
    void tearDown() {
        userRepository.deleteAll();
    }

    @Test
    void testFindAll() {
        List<UserDao> userDaos = userRepository.findAll();

        assertEquals(10, userDaos.size());
    }
}