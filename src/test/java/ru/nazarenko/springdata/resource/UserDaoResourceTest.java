package ru.nazarenko.springdata.resource;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import ru.nazarenko.springdata.config.AppConfiguration;
import ru.nazarenko.springdata.dto.UserDto;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
@ActiveProfiles(profiles = AppConfiguration.DEV_PROFILE)
@Transactional
class UserDaoResourceTest {

    private static final String ROOT_RESOURCE_PATH = "/users";

    @Autowired
    TestRestTemplate restTemplate;

    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>(
            AppConfiguration.DATABASE_DOCKER_IMAGE
    );

    @Test
     void shouldReturnSomeUsers() {
        UserDto[] userDaos = restTemplate.getForObject(ROOT_RESOURCE_PATH, UserDto[].class);
        assertEquals(1, userDaos.length);
    }

}