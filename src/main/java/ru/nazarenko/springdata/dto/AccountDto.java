package ru.nazarenko.springdata.dto;

import lombok.Data;
import ru.nazarenko.springdata.model.account.AccountType;

import java.io.Serializable;


@Data
public class AccountDto implements Serializable {
    private Long accountId;
    private AccountType accountType;
}
