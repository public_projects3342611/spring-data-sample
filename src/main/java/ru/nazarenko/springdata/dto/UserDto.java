package ru.nazarenko.springdata.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Data
public class UserDto implements Serializable {
    private Long userId;
    private String name;
    private String surname;
    private LocalDate dateOfBirth;
    private List<AccountDto> accounts;
}
