package ru.nazarenko.springdata.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.nazarenko.springdata.model.account.AccountDao;
import ru.nazarenko.springdata.service.AccountService;

import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountsResource {
    private final AccountService accountService;

    @GetMapping("/account")
    public AccountDao getAccountById(@RequestParam("id") Long accountId) {
        return accountService.getAccountById(accountId);
    }

    @GetMapping
    public List<AccountDao> getAllAccounts() {
        return accountService.getAllAccounts();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long createUser(@RequestBody AccountDao accountDao) {
        return accountService.createAccount(accountDao);
    }

    @PutMapping
    public Long updateUser(@RequestBody AccountDao accountDao) {
        return accountService.updateAccount(accountDao);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@RequestParam("id") Long accountId) {
        accountService.deleteAccountById(accountId);
    }

}
