package ru.nazarenko.springdata.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.nazarenko.springdata.dto.UserDto;
import ru.nazarenko.springdata.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserResource {

    private final UserService userService;

    @GetMapping("/user")
    public UserDto getUserById(@RequestParam("id") Long userId) {
        return userService.findUserById(userId);
    }

    @GetMapping
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long createUser(@RequestBody UserDto userDao) {
        return userService.createUser(userDao);
    }

    @PutMapping
    public Long updateUser(@RequestBody UserDto userDao) {
        return userService.updateUser(userDao);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@RequestParam("id") Long userId) {
        userService.deleteUserById(userId);
    }

}
