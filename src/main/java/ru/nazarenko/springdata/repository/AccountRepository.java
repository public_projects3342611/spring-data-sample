package ru.nazarenko.springdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nazarenko.springdata.model.account.AccountDao;

public interface AccountRepository extends JpaRepository<AccountDao,Long> {
   AccountDao findUserByAccountId(Long id);
}
