package ru.nazarenko.springdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.nazarenko.springdata.model.UserDao;


@Repository
public interface UserRepository extends JpaRepository<UserDao, Long> {

    UserDao findUserWithAccountsByUserId(@Param("userId") Long userId);

}
