package ru.nazarenko.springdata.model.account;


import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import ru.nazarenko.springdata.model.UserDao;

import java.time.Instant;

import static ru.nazarenko.springdata.config.AppConfiguration.DATABASE_SCHEMA;

@Entity
@Table(name = "account", schema = DATABASE_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long accountId;

    @Enumerated(EnumType.ORDINAL)
    private AccountType accountType;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = true)
    private UserDao userDao;

    @CreationTimestamp
    private Instant createdOn;

    @UpdateTimestamp
    private Instant updatedOn;

}
