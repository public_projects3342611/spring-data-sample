package ru.nazarenko.springdata.model.account;

public enum AccountType {
    DEBIT,
    CREDIT
}
