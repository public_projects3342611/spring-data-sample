package ru.nazarenko.springdata.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import ru.nazarenko.springdata.model.account.AccountDao;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import static ru.nazarenko.springdata.config.AppConfiguration.DATABASE_SCHEMA;


@Entity
@Table(name = "user", schema = DATABASE_SCHEMA)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    private String name;

    private String surname;

    private LocalDate dateOfBirth;

    @OneToMany(mappedBy = "userDao", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AccountDao> accountDaos;

    @CreationTimestamp
    private Instant createdOn;

    @UpdateTimestamp
    private Instant updatedOn;

}
