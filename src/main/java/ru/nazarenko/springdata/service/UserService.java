package ru.nazarenko.springdata.service;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.nazarenko.springdata.dto.UserDto;
import ru.nazarenko.springdata.model.UserDao;
import ru.nazarenko.springdata.repository.UserRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Cacheable(value = "users", key = "#userId")
    public UserDto findUserById(Long userId) {
        return convertToDto(userRepository.findUserWithAccountsByUserId(userId));
    }

    private UserDto convertToDto(UserDao userDao) {
        return userMapper.userDaoToUserDto(userDao);
    }

    public List<UserDto> getAllUsers() {
        List<UserDao> all = userRepository.findAll();
        return userMapper.userDaosToUserDtos(all);
    }

    public Long createUser(UserDto userDto) {
        UserDao userDao = userMapper.userDtoToUserDao(userDto);

        return userRepository.save(userDao).getUserId();
    }

    public Long updateUser(UserDto userDto) {
        UserDao userDao = userMapper.userDtoToUserDao(userDto);
        return userRepository.save(userDao).getUserId();
    }

    public void deleteUserById(Long userId) {
        userRepository.deleteById(userId);
    }
}