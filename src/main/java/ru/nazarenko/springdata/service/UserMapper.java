package ru.nazarenko.springdata.service;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import ru.nazarenko.springdata.dto.AccountDto;
import ru.nazarenko.springdata.dto.UserDto;
import ru.nazarenko.springdata.model.UserDao;
import ru.nazarenko.springdata.model.account.AccountDao;

import java.util.List;

@Mapper(componentModel="spring")
public interface UserMapper {

    @Mapping(source = "accountDaos", target = "accounts")
    UserDto userDaoToUserDto(UserDao userDao);

    @Mapping(source = "accounts", target = "accountDaos")
    UserDao userDtoToUserDao(UserDto userDto);

    List<UserDto> userDaosToUserDtos(List<UserDao> userDaos);

    @Named("accountDaoToAccountDto")
    AccountDto accountDaoToAccountDto(AccountDao accountDao);

    @IterableMapping(qualifiedByName = "accountDaoToAccountDto")
    List<AccountDto> mapAccountDaoSetToAccountDtoSet(List<AccountDao> accountDaos);
}
