package ru.nazarenko.springdata.service;

import org.springframework.stereotype.Service;
import ru.nazarenko.springdata.model.account.AccountDao;
import ru.nazarenko.springdata.repository.AccountRepository;

import java.util.List;

@Service
public record AccountService(AccountRepository accountRepository) {

    public AccountDao getAccountById(Long accountId) {
        return accountRepository.findUserByAccountId(accountId);
    }

    public List<AccountDao> getAllAccounts() {
        return accountRepository.findAll();
    }

    public Long createAccount(AccountDao accountDao) {
        return accountRepository.save(accountDao).getAccountId();
    }

    public Long updateAccount(AccountDao accountDao) {
        return accountRepository.save(accountDao).getAccountId();
    }

    public void deleteAccountById(Long accountId) {
        accountRepository.deleteById(accountId);
    }
}