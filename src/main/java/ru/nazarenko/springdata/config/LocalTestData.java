package ru.nazarenko.springdata.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.nazarenko.springdata.model.UserDao;
import ru.nazarenko.springdata.model.account.AccountDao;
import ru.nazarenko.springdata.model.account.AccountType;
import ru.nazarenko.springdata.repository.AccountRepository;
import ru.nazarenko.springdata.repository.UserRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import static ru.nazarenko.springdata.config.AppConfiguration.DEV_PROFILE;

@Component
@Order(1)
@Profile(DEV_PROFILE)
public record LocalTestData(UserRepository userRepository, AccountRepository accountRepository) {

    @Bean
    public CommandLineRunner initUsers() {
        return args -> {

            AccountDao accountDao1 = new AccountDao();
            accountDao1.setAccountType(AccountType.DEBIT);

            AccountDao accountDao2 = new AccountDao();
            accountDao2.setAccountType(AccountType.DEBIT);

            AccountDao accountDao3 = new AccountDao();
            accountDao3.setAccountType(AccountType.CREDIT);

            AccountDao accountDao4 = new AccountDao();
            accountDao4.setAccountType(AccountType.CREDIT);

            accountRepository.saveAll(Set.of(accountDao1, accountDao2));

            UserDao userDao1 = new UserDao();

            userDao1.setName("Vasilii");
            userDao1.setSurname("Nazarenko");
            userDao1.setDateOfBirth(LocalDate.now());

            userRepository.saveAll(List.of(userDao1));

            accountDao1.setUserDao(userDao1);
            accountDao2.setUserDao(userDao1);

            accountRepository.saveAll(Set.of(accountDao1, accountDao2));

//            Faker faker = new Faker();
//
//            for (int i = 0; i < 9; i++) {
//                UserDao user = new UserDao();
//
//                user.setName(faker.name().firstName());
//                user.setSurname(faker.name().lastName());
//                user.setDateOfBirth(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
//
//                userRepository.save(user);
//            }
        };
    }
}
