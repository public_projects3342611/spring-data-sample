package ru.nazarenko.springdata.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {
    public static final String DEV_PROFILE = "local";
    public static final String DATABASE_SCHEMA = "public";
    public static final String DATABASE_DOCKER_IMAGE = "postgres:15-alpine";

}
